#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <wait.h>
#include <sys/stat.h>
#include <ctype.h>
#include <time.h>

static  const  char *dirpath = "/home/kadigas/Documents";

char *anime = "Animeku_";
char *ian = "IAN_";
char *key = "INNUGANTENG";
char *namdosaq = "nam_do-saq_";

char* timeGetter()
{
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    char *time;
    time = (char *) malloc(100);
    sprintf(time, "%d%02d%02d-%02d:%02d:%02d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    return time;
}

// Mengembalikan index posisi titik 
int findExtension(char *path)
{
    int len = strlen(path);
    for(int i = len-1; i >= 0; i--)
    {
        if(path[i] == '.')
        {
            return i;
        }
    }
    return len;
}

// Mengembalikan index posisi slash
int findSlash(char *path, int offset)
{
    int len = strlen(path);
    for(int i = 0; i < len; i++)
    {
        if(path[i] == '/')
        {
            return i + 1;
        }
    }
    return offset;
}

// Menulis log
void writeLog(char *str, char *from, char *to, int mode)
{
    FILE *fptr;
	fptr = fopen("/home/kadigas/modul4/Wibu.log", "a+");
    if(mode == 1)
        fprintf(fptr, "%s %s\n", str, from);
    else if(mode == 2)
        fprintf(fptr, "%s %s --> %s\n", str, from, to);
    fclose(fptr);
}

// Menulis log
void writeLogSequel(char *str, char *from, char *to, int mode)
{
    FILE *fptr;
	fptr = fopen("/home/kadigas/modul4/hayolongapain_A06.log", "a+");
    
    // Dapatkan tanggal dan waktu
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    char time[25];
    sprintf(time, "::%d%02d%02d-%02d:%02d:%02d::", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    char level[10];
    char end[100];
    
    if(mode == 1)
    {
        strcpy(level, "INFO");
        sprintf(end, "%s::%s", str, from);
        fprintf(fptr, "%s%s%s\n", level, time, end);
    }
        
    else if(mode == 2)
    {
        strcpy(level, "INFO");
        sprintf(end, "%s::%s::%s", str, from, to);
        fprintf(fptr, "%s%s%s\n", level, time, end);
    }

    else if(mode == 3)
    {
        strcpy(level, "WARNING");
        sprintf(end, "%s::%s", str, from);
        fprintf(fptr, "%s%s%s\n", level, time, end);
    }

    fclose(fptr);
}

void encodeAtbash(char *path)
{
    if (!strcmp(path, ".") || !strcmp(path, "..")) return;
    
    int end = findExtension(path);
    int begin = findSlash(path, 0);
    
    for (int i = begin; i < end; i++){
        if (isalpha(path[i])){
            char temp = path[i];
            if(isupper(path[i]))
                temp -= 'A'; 
            else continue;
            
            temp = 25 - temp; 
            
            if(isupper(path[i]))
                temp += 'A';

            path[i] = temp;
        }
    }
}

void decodeAtbash(char *path)
{
    if (!strcmp(path, ".") || !strcmp(path, "..")) return;

    int end = findExtension(path);
    int begin = findSlash(path, end);
    
    for (int i = begin; i < end; i++){
        if (isalpha(path[i])){
            char temp = path[i];
            if(isupper(path[i]))
                temp -= 'A';
            else continue;
            
            temp = 25 - temp;
            
            if(isupper(path[i]))
                temp += 'A';
            
            path[i] = temp;
        }
    }
}

void encodeRot13(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, "..")) return;

    int end = findExtension(path);
    int begin = findSlash(path, 0);

	for (int i = begin; i < end; i++){
		if (path[i] != '/' && isalpha(path[i])){
			char temp = path[i];
			if(isupper(path[i])) 
                continue; 
            else
                temp -= 'a';
            
            temp = (temp + 13) % 26;
			
            if(isupper(path[i]))
                continue;
            else temp += 'a';
			
            path[i] = temp;
		}
	}
}

void decodeRot13(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, "..")) return;

    int end = findExtension(path);
    int begin = findSlash(path, end);
	
	for (int i = begin; i < end; i++){
		if (path[i] != '/' && isalpha(path[i])){
			char temp = path[i];
			if(isupper(path[i]))
                continue;
            else temp -= 'a';
            
            temp = (temp + 13) % 26;
			
            if(isupper(path[i])) continue;
            else 
                temp += 'a';
			
            path[i] = temp;
		}
	}
}

void encodeVignere(char *path){
	if (!strcmp(path, ".") || !strcmp(path, "..")) 
        return;
	
	int end = strlen(path);
    int mid = findExtension(path);
	int begin = findSlash(path, 0);

    printf("begin -> %d\n mid -> %d\n end -> %d\n", begin, mid, end);

    
	for (int i = begin; i < mid; i++){
		if (isalpha(path[i])){
			char msg_temp = path[i]; 
            char key_temp = key[(i-begin) % strlen(key)];
			if(isupper(path[i]))
            {
                msg_temp -= 'A'; 
                key_temp -= 'A'; 
                msg_temp = (msg_temp + key_temp) % 26; 
                msg_temp += 'A';
            }
			else
            { 
                msg_temp -= 'a'; 
                key_temp = tolower(key_temp) - 'a'; 
                msg_temp = (msg_temp + key_temp) % 26; 
                msg_temp += 'a';
            }
			path[i] = msg_temp;
		}
	}
    

    
    for (int i = mid; i < end; i++){
		if (isalpha(path[i])){
			char msg_temp = path[i]; 
            char key_temp = key[(i-mid-1) % strlen(key)];
			if(isupper(path[i]))
            {
                msg_temp -= 'A'; 
                key_temp -= 'A'; 
                msg_temp = (msg_temp + key_temp) % 26; 
                msg_temp += 'A';
            }
			else
            { 
                msg_temp -= 'a'; 
                key_temp = tolower(key_temp) - 'a'; 
                msg_temp = (msg_temp + key_temp) % 26; 
                msg_temp += 'a';
            }
			path[i] = msg_temp;
		}
	}
}

void decodeVignere(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, "..")) 
        return;
	
	int end = strlen(path);
    int mid = findExtension(path);
	int begin = findSlash(path, end);
	
	for (int i = begin; i < mid; i++){
		if (isalpha(path[i])){
			char msg_temp = path[i];
			char key_temp = key[(i-begin) % strlen(key)];
			if(isupper(path[i]))
            {
                msg_temp -= 'A'; 
                key_temp -= 'A';
                msg_temp = (msg_temp - key_temp + 26) % 26; 
                msg_temp += 'A';
            }
			else
            {
                msg_temp -= 'a'; 
                key_temp = tolower(key_temp) - 'a'; 
                msg_temp = (msg_temp - key_temp + 26) % 26; 
                msg_temp += 'a';
            }
			path[i] = msg_temp;
		}
	}

    for (int i = mid; i < end; i++){
		if (isalpha(path[i])){
			char msg_temp = path[i];
			char key_temp = key[(i-mid-1) % strlen(key)];
			if(isupper(path[i]))
            {
                msg_temp -= 'A'; 
                key_temp -= 'A';
                msg_temp = (msg_temp - key_temp + 26) % 26; 
                msg_temp += 'A';
            }
			else
            {
                msg_temp -= 'a'; 
                key_temp = tolower(key_temp) - 'a'; 
                msg_temp = (msg_temp - key_temp + 26) % 26; 
                msg_temp += 'a';
            }
			path[i] = msg_temp;
		}
	}
}

void getBiner(char *filename, char *bin, char *lowercase){

    int end = strlen(filename);
	int begin = findSlash(filename, 0);
    int i;
    for(i = begin; i <end; i++){
        if(isupper(filename[i])){
            bin[i] = '1';
            lowercase[i] = filename[i] + 32;
        }
        else{
            bin[i] = '0';
            lowercase[i] = filename[i];
        }
    }
    bin[end] = '\0';
    for(; i < strlen(filename); i++){
        lowercase[i] = filename[i];
    }
    lowercase[i] = '\0';
}

int convertBinToDec(char *bin){
	int temp = 1;
    int result = 0;
	for(int i=strlen(bin)-1; i>=0; i--){if(bin[i] == '1') result += temp; temp *= 2;}
	return result;
}

int convertToDec(char *ext){
	int dec = 0; 
    int multiplier = 1;
	for(int i=strlen(ext)-1; i>=0; i--)
    {
        dec += (ext[i]-'0')*multiplier;
        multiplier *= 10;
    }
	return dec;
}

void convertDecToBin(int dec, char *bin, int len){
	int idx = 0;
	while(dec){
		if(dec & 1) bin[idx] = '1';
		else bin[idx] = '0';idx++;
		dec /= 2;
	}
	while(idx < len){
		bin[idx] = '0'; idx++;
	}
	bin[idx] = '\0';
	
	for(int i=0; i<idx/2; i++){
		char sementara = bin[i];bin[i] = bin[idx-1-i];bin[idx-1-i] = sementara;
	}
}

void getDecimal(char *filename, char *bin, char *normalcase){
	int end = strlen(filename);
	int begin = findSlash(filename, 0);
	int i;
	
	for(i=begin; i<end; i++){
		if(bin[i-begin] == '1') normalcase[i-begin] = filename[i] - 32;
		else normalcase[i-begin] = filename[i];
	}
	
	for(; i<strlen(filename); i++){
		normalcase[i-begin] = filename[i];
	}
	normalcase[i-begin] = '\0';

}

void encryptBinary(char *path){
    chdir(path);
    
	DIR *dp;
	struct dirent *dir;
	struct stat sb;
	dp = opendir(".");
	if(dp == NULL) return;
	
	char dirPath[1000];
	char filePath[1000];
	char filePathBinary[1000];

    while ((dir = readdir(dp)) != NULL){
		if (stat(dir->d_name, &sb) < 0);
		else if (S_ISDIR(sb.st_mode)){
			if (strcmp(dir->d_name,".") == 0 || strcmp(dir->d_name,"..") == 0) continue;
            
            sprintf(dirPath,"%s/%s",path, dir->d_name);
            //Rekursif
            encryptBinary(dirPath);
		}
        else{
			sprintf(filePath,"%s/%s",path, dir->d_name);
			char bin[1000], lowercase[1000]; 

            getBiner(dir->d_name, bin, lowercase);
			int dec = convertBinToDec(bin);
			sprintf(filePathBinary,"%s/%s.%d",path,lowercase,dec); 
            
            rename(filePath, filePathBinary);
		}
	}
    closedir(dp);
}

void decodeBinary(char *path){
	chdir(path);

	DIR *dp;
	struct dirent *dir;
	struct stat lol;
	dp = opendir(".");
	if(dp == NULL) return;
	
	char dirPath[1000];
	char filePath[1000];
	char filePathDecimal[1000];
	
    while ((dir = readdir(dp)) != NULL){
		if (stat(dir->d_name, &lol) < 0);
		else if (S_ISDIR(lol.st_mode)){
			if (strcmp(dir->d_name,".") == 0 || strcmp(dir->d_name,"..") == 0) continue;
            sprintf(dirPath,"%s/%s",path, dir->d_name);
            decodeBinary(dirPath);
		}
		else{
			sprintf(filePath,"%s/%s",path, dir->d_name);
			char fname[1000], bin[1000], normalcase[1000], clearPath[1000];
			
			strcpy(fname, dir->d_name);
			char *ext = strrchr(fname, '.');
			int dec = convertToDec(ext+1);

			for(int i=0; i<strlen(fname)-strlen(ext); i++) 
                clearPath[i] = fname[i];
			
			char *ext2 = strrchr(clearPath, '.');
			convertDecToBin(dec, bin, strlen(clearPath)-strlen(ext2));

            getDecimal(clearPath, bin, normalcase);
            sprintf(filePathDecimal,"%s/%s",path,normalcase);
            
            rename(filePath, filePathDecimal);
		}
	}
    closedir(dp);
}

void encodeDirectorySpecial(char *path)
{
    if (!strcmp(path, ".") || !strcmp(path, "..")) 
        return;
    
    int end = strlen(path);
	int begin = findSlash(path, 0);

	for (int i = begin; i < end; i++){
		char *isAnime = strstr(path[i],anime);
        char *isIAN = strstr(path[i],ian);

        if(isAnime != NULL){
            encodeAtbash(path[i]);
            encodeRot13(path[i]);
        }
        else if(isIAN != NULL){
            encodeVignere(path[i]);
        }
        else{
            encryptBinary(path[i]);
        }
	}
}

void decodeDirectorySpecial(char *path)
{
    if (!strcmp(path, ".") || !strcmp(path, "..")) 
        return;
    
    int end = strlen(path);
	int begin = findSlash(path, 0);

	for (int i = begin; i < end; i++){
		char *isAnime = strstr(path[i],anime);
        char *isIAN = strstr(path[i],ian);

        if(isAnime != NULL){
            decodeAtbash(path[i]);
            decodeRot13(path[i]);
        }
        else if(isIAN != NULL){
            decodeVignere(path[i]);
        }
        else{
            decodeBinary(path[i]);
        }
	}
}

static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1024];

    char *satu = strstr(path, anime);
    if (satu != NULL) {
        decodeRot13(satu);
        decodeAtbash(satu);
    }

    char *dua = strstr(path, ian);
    if (dua != NULL) decodeVignere(dua);

    //Nomor 3
    char *tiga = strstr(path,namdosaq);
    if(tiga != NULL){
        decodeDirectorySpecial(tiga);
    }
    
    if (strcmp(path, "/") == 0){
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1024];

    char *satu = strstr(path, anime);
    if (satu != NULL) {
        decodeAtbash(satu);
        decodeRot13(satu);
    }

    char *dua = strstr(path, ian);
    if (dua != NULL) decodeVignere(dua);

    //Nomor 3
    char *tiga = strstr(path,namdosaq);
    if(tiga != NULL){
        decodeDirectorySpecial(tiga);
    }

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        if(strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) continue;
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if (satu != NULL) {
            encodeAtbash(de->d_name);
            encodeRot13(de->d_name);
        }

        if (dua != NULL) encodeVignere(de->d_name);

        if(tiga != NULL) encodeDirectorySpecial(de->d_name);

        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}


static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1024];

    char *satu = strstr(path, anime);
    if (satu != NULL) {
        decodeAtbash(satu);
        decodeRot13(satu);
    }

    char *dua = strstr(path, ian);
    if (dua != NULL) decodeVignere(dua);

    //Nomor 3
    char *tiga = strstr(path,namdosaq);
    if(tiga != NULL){
        decodeDirectorySpecial(tiga);
    }

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
	int res;

    char fpath[1024];

    char *satu = strstr(path, anime);
    if (satu != NULL) {
        decodeAtbash(satu);
        decodeRot13(satu);
    }

    char *dua = strstr(path, ian);
    if (dua != NULL) decodeVignere(dua);

    //Nomor 3
    char *tiga = strstr(path,namdosaq);
    if(tiga != NULL){
        decodeDirectorySpecial(tiga);
    }

    if (strcmp(path, "/") == 0){
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);

	res = mkdir(path, mode);

    if(satu != NULL) writeLog("MKDIR terenkripsi", fpath, "", 1);
    else if (dua != NULL) writeLogSequel("MKDIR", fpath, "", 1);


	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_rename(const char *from, const char *to)
{
	int res;
    char fromDir[1024], toDir[1024];

    char *satu = strstr(to, anime);
    if (satu != NULL) {
        decodeAtbash(satu);
        decodeRot13(satu);
    }

    char *dua = strstr(to, ian);
    if (dua != NULL) decodeVignere(dua);

    //Nomor 3
    char *tiga = strstr(to,namdosaq);
    if(tiga != NULL){
        decodeDirectorySpecial(tiga);
    }

    sprintf(fromDir, "%s%s", dirpath, from);
    sprintf(toDir, "%s%s", dirpath, to);

    if(strstr(toDir, anime) != NULL) writeLog("RENAME terenkripsi", fromDir, toDir, 2);
    else if(strstr(fromDir, anime) != NULL) writeLog("RENAME terdecode", fromDir, toDir, 2);

    if (dua != NULL || strstr(from, ian) != NULL) writeLogSequel("RENAME", fromDir, toDir, 2);

	res = rename(fromDir, toDir);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_rmdir(const char *path)
{
	int res;
    char fpath[1024];
    
    if (strcmp(path, "/") == 0){
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);

    char *dua = strstr(path, ian);
    if (dua != NULL) writeLogSequel("RMDIR", fpath, "", 3);

    //Nomor 3
    char *tiga = strstr(path,namdosaq);
    if(tiga != NULL){
        decodeDirectorySpecial(tiga);
    }

	res = rmdir(path);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_unlink(const char *path)
{
	int res;
    char fpath[1024];

    if (strcmp(path, "/") == 0){
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);

    char *dua = strstr(path, ian);
    if (dua != NULL) writeLogSequel("UNLINK", fpath, "", 3);

    //Nomor 3
    char *tiga = strstr(path,namdosaq);
    if(tiga != NULL){
        decodeDirectorySpecial(tiga);
    }

	res = unlink(path);
	if (res == -1)
		return -errno;

	return 0;
}


static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .unlink	= xmp_unlink,
	.rmdir	= xmp_rmdir,
};



int  main(int  argc, char *argv[])
{
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}

/* Referensi
 * https://www.cs.hmc.edu/~geoff/classes/hmc.cs135.201109/homework/fuse/fusexmp.c 
 * https://www.base64encode.org/
 * https://rot13.com/
 * youtube.com/watch?v=b1SmB2Myfxk
 * https://codereview.stackexchange.com/questions/81886/rot13-algorithm-in-c
 * https://github.com/randerson112358/C-Programs/blob/master/vigenereCipher.c
 * https://www.programmingalgorithms.com/algorithm/vigenere-cipher/c/ 
 * https://www.dcode.fr/vigenere-cipher
 */