# soal-shift-sisop-modul-4-A06-2022

## Soal no. 1
Pada soal nomor 1, kami diminta untuk mengecek apakah sebuah direktori mengandung string ```Animeku_``` pada awal nama direktorinya. Maka, pertama-tama, agar nantinya tidak terjadi pengulangan penulisan ```Animeku_```, maka kami menyimpannya ke dalam sebuah variable string terlebih dahulu. Selain itu, karena akan menggunakan fuse, maka perlu dilakukan pemasangan root yang sesuai soal minta, yaitu pada ```home/[user]/Documents```.

```bash
static  const  char *dirpath = "/home/kadigas/Documents";

char *anime = "Animeku_";
```
Jika pada file terdapat string ```Animeku_``` pada awal nama direktorinya, maka semua file yang ada didalamnya akan diencode dengan syarat encode ```atbash``` untuk huruf besar atau uppercase, dan ```rot13``` untuk huruf kecil atau lowercase. Selain itu, ekstensi dari file tidak perlu di-encode. Untuk itu, pertama-tama, kita akan mencari terlebih dahulu index untuk ```.``` dari sebuah file dari belakang, yang mana merupakan tanda bahwa setelahnya adalah ekstensi dari file tersebut. Selain itu, kita akan mencari ```/``` yang akan menjadi begin point untuk pengecekan nanti.

```bash
int findExtension(char *path)
{
    int len = strlen(path);
    for(int i = len-1; i >= 0; i--)
    {
        if(path[i] == '.')
        {
            return i;
        }
    }
    return len;
}

int findSlash(char *path, int offset)
{
    int len = strlen(path);
    for(int i = 0; i < len; i++)
    {
        if(path[i] == '/')
        {
            return i + 1;
        }
    }
    return offset;
}
```
Setelah itu, terdapat fungsi encode dan decode untuk masing-masing ```atbash``` dan ```rot13```:

#### Fungsi Encode dan Decode Atbash

```bash
void encodeAtbash(char *path)
{
    if (!strcmp(path, ".") || !strcmp(path, "..")) return;
    
    int end = findExtension(path);
    int begin = findSlash(path, 0);
    
    for (int i = begin; i < end; i++){
        if (isalpha(path[i])){
            char temp = path[i];
            if(isupper(path[i]))
                temp -= 'A'; 
            else continue;
            
            temp = 25 - temp; 
            
            if(isupper(path[i]))
                temp += 'A';

            path[i] = temp;
        }
    }
}

void decodeAtbash(char *path)
{
    if (!strcmp(path, ".") || !strcmp(path, "..")) return;

    int end = findExtension(path);
    int begin = findSlash(path, end);
    
    for (int i = begin; i < end; i++){
        if (isalpha(path[i])){
            char temp = path[i];
            if(isupper(path[i]))
                temp -= 'A';
            else continue;
            
            temp = 25 - temp;
            
            if(isupper(path[i]))
                temp += 'A';
            
            path[i] = temp;
        }
    }
}
```
Fungsi ```atbash``` ini akan melakukan pengecekan dari begin point (jika encode dilakukan dari return value fungsi ```findSlash(path, 0)```, sedangkan decode dilakukan dari return value fungsi ```findSlash(path, end)```) dan end point yaitu sebelum ekstensi. Selain itu, pada fungsi ```atbash``` ini, program akan melakukan pengecakan untuk alphabet dengan ```isalpha()```, kemudian uppercase dengan ```isupper()```. Apabila merupakan uppercase, maka dilakukan encode dan decode ```atbash```, jika tidak, maka ```for loop``` akan ```continue``` ke huruf selanjutnya.

Dengan menggunakan flow yang sama, fungsi encode dan decode ```rot13```:

### Fungsi Encode dan Deode Rot13

```bash
void encodeRot13(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, "..")) return;

    int end = findExtension(path);
    int begin = findSlash(path, 0);

	for (int i = begin; i < end; i++){
		if (path[i] != '/' && isalpha(path[i])){
			char temp = path[i];
			if(isupper(path[i])) 
                continue; 
            else
                temp -= 'a';
            
            temp = (temp + 13) % 26;
			
            if(isupper(path[i]))
                continue;
            else temp += 'a';
			
            path[i] = temp;
		}
	}
}

void decodeRot13(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, "..")) return;

    int end = findExtension(path);
    int begin = findSlash(path, end);
	
	for (int i = begin; i < end; i++){
		if (path[i] != '/' && isalpha(path[i])){
			char temp = path[i];
			if(isupper(path[i]))
                continue;
            else temp -= 'a';
            
            temp = (temp + 13) % 26;
			
            if(isupper(path[i])) continue;
            else 
                temp += 'a';
			
            path[i] = temp;
		}
	}
}
```
di mana pada kedua fungsi ini, operasinya akan berjalan apabila ditemukan alphabet lowercase pada nama file. Jika tidak, maka ```for loop``` akan continue ke karakter selanjutnya.

Kemudian, untuk menjalankan fungsi-fungsi di atas, akan digunakan FUSE dengan operasi di bawah ini:

```bash
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .unlink	= xmp_unlink,
	.rmdir	= xmp_rmdir,
};

int  main(int  argc, char *argv[])
{
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}
```
Sehingga, fungsi-fungsi yang dipakai dalam ```fuse_operations``` untuk soal 1:

```bash
static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1024];

    char *satu = strstr(path, anime);
    if (satu != NULL) {
        decodeRot13(satu);
        decodeAtbash(satu);
    }

   ...
    
    if (strcmp(path, "/") == 0){
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1024];

    char *satu = strstr(path, anime);
    if (satu != NULL) {
        decodeAtbash(satu);
        decodeRot13(satu);
    }

   ...

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        if(strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) continue;
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if (satu != NULL) {
            encodeAtbash(de->d_name);
            encodeRot13(de->d_name);
        }

       ...

        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}


static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1024];

    char *satu = strstr(path, anime);
    if (satu != NULL) {
        decodeAtbash(satu);
        decodeRot13(satu);
    }

    ...

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
	int res;

    char fpath[1024];

    char *satu = strstr(path, anime);
    if (satu != NULL) {
        decodeAtbash(satu);
        decodeRot13(satu);
    }

    ...

    if (strcmp(path, "/") == 0){
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);

	res = mkdir(path, mode);

    if(satu != NULL) writeLog("MKDIR terenkripsi", fpath, "", 1);
    
	...

	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_rename(const char *from, const char *to)
{
	int res;
    char fromDir[1024], toDir[1024];

    char *satu = strstr(to, anime);
    if (satu != NULL) {
        decodeAtbash(satu);
        decodeRot13(satu);
    }

    ...

    sprintf(fromDir, "%s%s", dirpath, from);
    sprintf(toDir, "%s%s", dirpath, to);

    if(strstr(toDir, anime) != NULL) writeLog("RENAME terenkripsi", fromDir, toDir, 2);
    else if(strstr(fromDir, anime) != NULL) writeLog("RENAME terdecode", fromDir, toDir, 2);

    if (dua != NULL || strstr(from, ian) != NULL) writeLogSequel("RENAME", fromDir, toDir, 2);

	res = rename(fromDir, toDir);
	if (res == -1)
		return -errno;

	return 0;
}
```
Untuk soal 1b dan 1c, maka fokus kita ada pada fungsi ```xmp_rename()```. Dikarenakan adanya dua parameter, yaitu ```from``` dan ```to```, maka kami membuat variable untuk handling yaitu ```fromDir``` dan ```toDir```. Program akan mengecek apakah path ```to```, yang merupakan hasil akhir dari rename, mengandung ```Animeku_``` di dalamnya (dilakukan dengan menggunakan ```strstr()```). Jika iya, maka akan dijalankan fungsi ```decodeAtbash``` dan ```decodeRot13``` dengan mempassing string tersebut. Setelah itu, kita simpan ```dirpath``` dan ```from``` ke dalam ```fromDir```, serta ```dirpath``` dan ```to``` ke dalam ```toDir```. 

Untuk soal 1d, setelah rename dan decode selesai, maka kita perlu menuliskannya ke dalam log, yaitu ```Wibu.log```. Di sini, kita akan melakukan pengecekan. Apabila dalam ```toDir``` ditemukan ```Animeku_```, maka log akan ditulis ```RENAME terenkripsi```, kemudian ```fromDir --> toDir```. Jika tidak, maka log akan ditulis ```RENAME terdecode```, kemudian ```fromDir --> toDir```. Untuk itu, kita perlu membuat fungsi ```writeLog```:

```bash
void writeLog(char *str, char *from, char *to, int mode)
{
    FILE *fptr;
	fptr = fopen("/home/kadigas/modul4/Wibu.log", "a+");
    if(mode == 1)
        fprintf(fptr, "%s %s\n", str, from);
    else if(mode == 2)
        fprintf(fptr, "%s %s --> %s\n", str, from, to);
    fclose(fptr);
}
```
mode 1 digunakan apabila kita sedang membuat direktori (menjalankan fungsi ```xmp_mkdir()```) yang mana hanya membutuhkan ```str``` dan satu path (dalam fungsi ini disimpan oleh ```from```), sedangkan mode 2 digunakan apabila kita merename suatu direktori (menjalankan fungsi ```xmp_rename```).

Untuk soal 1e, fungsi encode dan decode ```atbash``` dan ```rot13``` akan berjalan secara rekursif, hal ini dapat dilihat pada fungsi ```xmp_readdir```.

### Screenshots:
<img src="./screenshot/soal1/1.png" alt="">
<img src="./screenshot/soal1/2.png" alt="">
<img src="./screenshot/soal1/3.png" alt="">
<img src="./screenshot/soal1/4.png" alt="">
<img src="./screenshot/soal1/5.png" alt="">
<img src="./screenshot/soal1/6.png" alt="">

### Kendala
Dikarenakan kurangnya pemahaman lebih dalam tentang FUSE, banyak terjadi trial error pada saat pengerjaan. Selain itu, terjadi pula permission denied ketika membuat direktori secara langsung pada FUSE setelah melakukan pull dari gitlab meskipun sudah allow_others pada /etc/fuse.conf, add user dalam group fuse, maupun chmod.

## Soal no. 2
Soal nomor 2 melibatkan Vigener Cipher dengan key "INNUGANTENG". Dalam Vigenere Cipher, tiap abjad A-Z memiliki value 0-25. 
Cara kerja encoding Vigenere Cipher adalah mengambil value abjad dari message, lalu dijumlahkan dengan value abjad dari key. Setelah itu, akan dilihat abjad apa yang value-nya sesuai dengan hasil penjumlahan tersebut. 
Misalkan message-nya "Billie" dan key-nya "INNUGANTENG". Abjad pertama dari message adalah 'B' yang bernilai 1. Abjad pertama dari key adalah 'I' yang bernilai 8. Jika ditambahkan, hasilnya 9 yang merupakan value dari 'J'.
Berikut fungsi yang digunakan untuk encoding Vigenere.

#### Fungsi Encode Vigenere
```c
void encodeVignere(char *path){
	if (!strcmp(path, ".") || !strcmp(path, "..")) 
        return;
	
	int end = strlen(path);
    int mid = findExtension(path);
	int begin = findSlash(path, 0);

    printf("begin -> %d\n mid -> %d\n end -> %d\n", begin, mid, end);

    
	for (int i = begin; i < mid; i++){
		if (isalpha(path[i])){
			char msg_temp = path[i]; 
            char key_temp = key[(i-begin) % strlen(key)];
			if(isupper(path[i]))
            {
                msg_temp -= 'A'; 
                key_temp -= 'A'; 
                msg_temp = (msg_temp + key_temp) % 26; 
                msg_temp += 'A';
            }
			else
            { 
                msg_temp -= 'a'; 
                key_temp = tolower(key_temp) - 'a'; 
                msg_temp = (msg_temp + key_temp) % 26; 
                msg_temp += 'a';
            }
			path[i] = msg_temp;
		}
	}
    

    
    for (int i = mid; i < end; i++){
		if (isalpha(path[i])){
			char msg_temp = path[i]; 
            char key_temp = key[(i-mid-1) % strlen(key)];
			if(isupper(path[i]))
            {
                msg_temp -= 'A'; 
                key_temp -= 'A'; 
                msg_temp = (msg_temp + key_temp) % 26; 
                msg_temp += 'A';
            }
			else
            { 
                msg_temp -= 'a'; 
                key_temp = tolower(key_temp) - 'a'; 
                msg_temp = (msg_temp + key_temp) % 26; 
                msg_temp += 'a';
            }
			path[i] = msg_temp;
		}
	}
}
```
Untuk decoding Vigenere Cipher, caranya mayoritas sama dengan encoding, bedanya encoding menjumlahkan value abjad dari message dan key, sedangkan decoding melakukan pengurangan value antara kedua abjad tersebut. Berikut fungsi yang digunakan untuk decoding Vigenere.

#### Fungsi Decode Vigenere
```c
void decodeVignere(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, "..")) 
        return;
	
	int end = strlen(path);
    int mid = findExtension(path);
	int begin = findSlash(path, end);
	
	for (int i = begin; i < mid; i++){
		if (isalpha(path[i])){
			char msg_temp = path[i];
			char key_temp = key[(i-begin) % strlen(key)];
			if(isupper(path[i]))
            {
                msg_temp -= 'A'; 
                key_temp -= 'A';
                msg_temp = (msg_temp - key_temp + 26) % 26; 
                msg_temp += 'A';
            }
			else
            {
                msg_temp -= 'a'; 
                key_temp = tolower(key_temp) - 'a'; 
                msg_temp = (msg_temp - key_temp + 26) % 26; 
                msg_temp += 'a';
            }
			path[i] = msg_temp;
		}
	}

    for (int i = mid; i < end; i++){
		if (isalpha(path[i])){
			char msg_temp = path[i];
			char key_temp = key[(i-mid-1) % strlen(key)];
			if(isupper(path[i]))
            {
                msg_temp -= 'A'; 
                key_temp -= 'A';
                msg_temp = (msg_temp - key_temp + 26) % 26; 
                msg_temp += 'A';
            }
			else
            {
                msg_temp -= 'a'; 
                key_temp = tolower(key_temp) - 'a'; 
                msg_temp = (msg_temp - key_temp + 26) % 26; 
                msg_temp += 'a';
            }
			path[i] = msg_temp;
		}
	}
}
```
Dalam kedua fungsi di atas, encoding dan decoding filename dan extension dilakukan secara terpisah agar hasil sesuai dengan permintaan soal. For loop pertama digunakan untuk meng-encode/men-decode filename, sedangkan for loop kedua digunakan untuk extension-nya. Setelah membahas tentang dasar-dasar Vigenere Cipher, selanjutnya akan dibahas tentang soalnya. 

Kita diminta untuk meng-encode isi direktori dengan Vigenere Cipher jika dibuat atau di-rename dengan awalan "IAN_[nama]" (2a - 2b). Jika awalan dihilangkan pada saat rename, maka isi akan kembali ter-decode (2c). Berikut fungsi-fungsi yang digunakan:
### Fungsi getattr
```c
static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1024];

    ...

    char *dua = strstr(path, ian);
    if (dua != NULL) decodeVignere(dua);

    ...
    
    if (strcmp(path, "/") == 0){
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}
```
### Fungsi readdir
```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1024];

    ...

    char *dua = strstr(path, ian);
    if (dua != NULL) decodeVignere(dua);

    ...

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        if(strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) continue;
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        ...

        if (dua != NULL) encodeVignere(de->d_name);

        ...

        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}
```

### Fungsi read
```c
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1024];

    ...

    char *dua = strstr(path, ian);
    if (dua != NULL) decodeVignere(dua);

    ...

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}
```

### Fungsi mkdir
```c
static int xmp_mkdir(const char *path, mode_t mode)
{
	int res;

    char fpath[1024];

    ...

    char *dua = strstr(path, ian);
    if (dua != NULL) decodeVignere(dua);

    ...

    if (strcmp(path, "/") == 0){
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);

	res = mkdir(path, mode);

    ...

    ...


	if (res == -1)
		return -errno;

	return 0;
}
```

### Fungsi rename
```c
static int xmp_rename(const char *from, const char *to)
{
	int res;
    char fromDir[1024], toDir[1024];

    ...

    char *dua = strstr(to, ian);
    if (dua != NULL) decodeVignere(dua);

    ...

    sprintf(fromDir, "%s%s", dirpath, from);
    sprintf(toDir, "%s%s", dirpath, to);

    ...

    ...

	res = rename(fromDir, toDir);
	if (res == -1)
		return -errno;

	return 0;
}
```
Digunakan fungsi strstr untuk mendeteksi apakah ada awalan "IAN_" dalam nama folder. Jika ada, maka akan di-encode/di-decode dengan Vigenere.

Berikut beberapa gambar hasil untuk 2a, 2b, 2c:

<img src="./screenshot/soal2/01.png" alt="Folder IAN_kwetiaw di Documents">
<img src="./screenshot/soal2/02.png" alt="Folder IAN_kwetiaw di fuse">
<img src="./screenshot/soal2/03.png" alt="Mencoba rename menghilangkan awalan IAN_">
<img src="./screenshot/soal2/04.png" alt="Isi folder kwetiaw ter-decode">
<img src="./screenshot/soal2/05.png" alt="Isi folder IAN_testings yang ada dalam kwetiaw, masih ter-encode">

Selanjutnya diminta membuat log dengan nama "hayolongapain_[kelompok].log" yang berisi daftar perintah system call yang dijalankan pada filesystem (2d). Ada dua level:

- INFO: Untuk system call rename dan mkdir
- WARNING: Untuk system call rmdir dan unlink

Format pencatatannya adalah:

```
[Level]::[dd][mm][yyyy]-[HH]:[MM]:[SS]::[CMD]::[DESC :: DESC]
```
Dengan keterangan:

```
- Level : Level logging
- dd : Tanggal
- mm : Bulan
- yyyy : Tahun
- HH : Jam (dengan format 24 Jam)
- MM : Menit
- SS : Detik
- CMD : System call yang terpanggil
- DESC : Informasi dan parameter tambahan 
```
(2e)

Fungsi yang digunakan untuk menulis log adalah sebagai berikut:
### Fungsi Menulis Log
```c
void writeLogSequel(char *str, char *from, char *to, int mode)
{
    FILE *fptr;
	fptr = fopen("/home/illumi/modul4/hayolongapain_A06.log", "a+");
    
    // Dapatkan tanggal dan waktu
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    char time[25];
    sprintf(time, "::%d%02d%02d-%02d:%02d:%02d::", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    char level[10];
    char end[100];
    
    if(mode == 1)
    {
        strcpy(level, "INFO");
        sprintf(end, "%s::%s", str, from);
        fprintf(fptr, "%s%s%s\n", level, time, end);
    }
        
    else if(mode == 2)
    {
        strcpy(level, "INFO");
        sprintf(end, "%s::%s::%s", str, from, to);
        fprintf(fptr, "%s%s%s\n", level, time, end);
    }

    else if(mode == 3)
    {
        strcpy(level, "WARNING");
        sprintf(end, "%s::%s", str, from);
        fprintf(fptr, "%s%s%s\n", level, time, end);
    }

    fclose(fptr);
}
```
Fungsi writeLogSequel mempunyai 4 parameter:
- str nantinya akan berisi system call (MKDIR, RENAME, RMDIR, UNLINK)
- from akan berisi path direktori, dalam kasus rename, from akan berisi nama awal direktori
- to hanya digunakan dalam kasus rename, akan berisi nama akhir direktori
- mode 1 berarti mkdir, mode 2 berarti rename, mode 3 berarti rmdir dan unlink

Fungsi akan memasukkan tanggal dan waktu system call ke dalam string time. Sesuai namanya, string level berisi level INFO atau WARNING. String end berisi system call dan direktori yang terlibat. Setelah ketiga string telah terisi, akan di-print ke dalam file log. Berikut adalah pemanggilan fungsi writeLogSequel dalam fungsi mkdir, rename, rmdir, unlink:

### Fungsi mkdir
```c
static int xmp_mkdir(const char *path, mode_t mode)
{
	int res;

    char fpath[1024];

    ...

    char *dua = strstr(path, ian);
    if (dua != NULL) decodeVignere(dua);

    ...

    if (dua != NULL) writeLogSequel("MKDIR", fpath, "", 1);
    ...

}
```

### Fungsi rename
```c
static int xmp_rename(const char *from, const char *to)
{
	int res;
    char fromDir[1024], toDir[1024];

    ...

    char *dua = strstr(to, ian);
    if (dua != NULL) decodeVignere(dua);

    ...

    if (dua != NULL || strstr(from, ian) != NULL) writeLogSequel("RENAME", fromDir, toDir, 2);
    
    ...
}
```
### Fungsi rmdir
```c
static int xmp_rmdir(const char *path)
{
	int res;
    char fpath[1024];
    
    if (strcmp(path, "/") == 0){
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);

    char *dua = strstr(path, ian);
    if (dua != NULL) writeLogSequel("RMDIR", fpath, "", 3);

    ...

	...
}
```

### Fungsi unlink
```c
static int xmp_unlink(const char *path)
{
	int res;
    char fpath[1024];

    if (strcmp(path, "/") == 0){
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);

    char *dua = strstr(path, ian);
    if (dua != NULL) writeLogSequel("UNLINK", fpath, "", 3);

    //Nomor 3
    char *tiga = strstr(path,namdosaq);
    if(tiga != NULL){
        decodeDirectorySpecial(tiga);
    }

	res = unlink(path);
	if (res == -1)
		return -errno;

	return 0;
}
```

Berikut gambar hasil untuk 2e:
<img src="./screenshot/soal2/08.png" alt="File log hayolongapain_A06.log">

### Kendala
- Untuk system call mkdir, rmdir, dan unlink bisa tertulis di log, tapi kalau dilakukan di fuse, selalu permission denied. Sudah coba pakai chmod 777 dan chmod 775 di folder tempat mount. Sudah coba edit etc/fuse.conf, nge-uncomment user_allow_other. Sudah coba mount pakai -o allow_other.
<img src="./screenshot/soal2/07.png" alt="File log hayolongapain_A06.log">


## Soal no. 3
pada nomor 3 kita disuruh membuat direktori spesial pada suatu folder yg diawali dengan ```nam_do-sa_q```, semisal kita menemukan folder tersebut, nanti kita mengembalikan enkripsi dari direktori ```Animeku``` maupun ```IAN```. Untuk mengenkripsi folder ```nam_do-sa_q```, ada fungsi namanya ```encodeDirectorySpecial``` yang mana nanti semisal kita menemukan path yang berawalan ```Animeku```, maka akan mengenkripsi sesuai dengan Animeku, dan ```IAN``` mengenkripsi sesuai dengan IAN, semisal tidak maka akan mengubah nama file menjadi uppercase insensitive dan diberi ekstensi baru berupa nilai desimal dari biner perbedaan namanya

```bash
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <wait.h>
#include <sys/stat.h>
#include <ctype.h>
#include <time.h>

static  const  char *dirpath = "/home/illumi/Documents";

char *anime = "Animeku_";
char *ian = "IAN_";
char *key = "INNUGANTENG";
char *namdosaq = "nam_do-saq_";

//kode lain ...

void encodeDirectorySpecial(char *path)
{
    if (!strcmp(path, ".") || !strcmp(path, "..")) 
        return;
    
    int end = strlen(path);
	int begin = findSlash(path, 0);

	for (int i = begin; i < end; i++){
		char *isAnime = strstr(path[i],anime);
        char *isIAN = strstr(path[i],ian);

        if(isAnime != NULL){
            encodeAtbash(path[i]);
            encodeRot13(path[i]);
        }
        else if(isIAN != NULL){
            encodeVignere(path[i]);
        }
        else{
            encryptBinary(path[i]);
        }
	}
}
void decodeDirectorySpecial(char *path)
{
    if (!strcmp(path, ".") || !strcmp(path, "..")) 
        return;
    
    int end = strlen(path);
	int begin = findSlash(path, 0);

	for (int i = begin; i < end; i++){
		char *isAnime = strstr(path[i],anime);
        char *isIAN = strstr(path[i],ian);

        if(isAnime != NULL){
            decodeAtbash(path[i]);
            decodeRot13(path[i]);
        }
        else if(isIAN != NULL){
            decodeVignere(path[i]);
        }
        else{
            decodeBinary(path[i]);
        }
	}
}
//kode lain ...
```

semisal menghilangkan awalan ```nam_do-sa_q```, maka akan menjadi direktori biasa dengan cara mengaktifkan fungsi ```decodeDirectorySpecial```. Terus untuk mengubah nama file menjadi uppercase insensitive dan diberi ekstensi baru berupa nilai desimal dari biner perbedaan namanya, kita gunakan fungsi ```encryptBinary```.

```bash
void encryptBinary(char *path){
    chdir(path);
    
	DIR *dp;
	struct dirent *dir;
	struct stat sb;
	dp = opendir(".");
	if(dp == NULL) return;
	
	char dirPath[1000];
	char filePath[1000];
	char filePathBinary[1000];

    while ((dir = readdir(dp)) != NULL){
		if (stat(dir->d_name, &sb) < 0);
		else if (S_ISDIR(sb.st_mode)){
			if (strcmp(dir->d_name,".") == 0 || strcmp(dir->d_name,"..") == 0) continue;
            
            sprintf(dirPath,"%s/%s",path, dir->d_name);
            //Rekursif
            encryptBinary(dirPath);
		}
        else{
			sprintf(filePath,"%s/%s",path, dir->d_name);
			char bin[1000], lowercase[1000]; 

            getBiner(dir->d_name, bin, lowercase);
			int dec = convertBinToDec(bin);
			sprintf(filePathBinary,"%s/%s.%d",path,lowercase,dec); 
            
            rename(filePath, filePathBinary);
		}
	}
    closedir(dp);
}
```

pada fungsi ```encryptBinary```, jika ```path``` berupa directory, maka akan dilakukan rekursif dengan memanggil kembali fumgsi ```encryptBinary```. semisal berupa file, maka nanti akan memanggil fungsi ```getBiner```, terus mengubah variabel bin ke decimal , dengan memanggil fungsi ```convertBinToDec``` setelah itu merename path yang lama menjadi path yang telah ditambah nilai desimal

```bash
void getBiner(char *filename, char *bin, char *lowercase){

    int end = strlen(filename);
	int begin = findSlash(filename, 0);
    int i;
    for(i = begin; i <end; i++){
        if(isupper(filename[i])){
            bin[i] = '1';
            lowercase[i] = filename[i] + 32;
        }
        else{
            bin[i] = '0';
            lowercase[i] = filename[i];
        }
    }
    bin[end] = '\0';
    for(; i < strlen(filename); i++){
        lowercase[i] = filename[i];
    }
    lowercase[i] = '\0';
}

int convertBinToDec(char *bin){
	int temp = 1;
    int result = 0;
	for(int i=strlen(bin)-1; i>=0; i--){if(bin[i] == '1') result += temp; temp *= 2;}
	return result;
}
```

di fungsi ```getBiner``` kita mengubah huruf huruf yang ada pada sebuah file, semisal terdapat huruf kapital, maka akan diubah menjadi huruf kecil dengan menambahkan angka 32, dan di fungsi ```convertBinToDec```, kita mengubah variabel bin yang telah dibuat menjadi angka decimal

Saat kita mengdecode folder ```nam_do-saq_```, kita memanggil fungsi ```decodeBinary``` yang mana nanti kita cek seperti fungsi ```encodebinary```, bedanya disini saat kita mendecode filenya, kita memiliki variabel ```ext``` yang mana memanggil fungsi ```strrchr``` untuk mendeteksi string selanjutnya abis tanda titik terakhir terus kita mengubah variabel ext tersebut dengan memanggil fungsi ```convertToDec```.

```bash
void decodeBinary(char *path){
	chdir(path);

	DIR *dp;
	struct dirent *dir;
	struct stat lol;
	dp = opendir(".");
	if(dp == NULL) return;
	
	char dirPath[1000];
	char filePath[1000];
	char filePathDecimal[1000];
	
    while ((dir = readdir(dp)) != NULL){
		if (stat(dir->d_name, &lol) < 0);
		else if (S_ISDIR(lol.st_mode)){
			if (strcmp(dir->d_name,".") == 0 || strcmp(dir->d_name,"..") == 0) continue;
            sprintf(dirPath,"%s/%s",path, dir->d_name);
            decodeBinary(dirPath);
		}
		else{
			sprintf(filePath,"%s/%s",path, dir->d_name);
			char fname[1000], bin[1000], normalcase[1000], clearPath[1000];
			
			strcpy(fname, dir->d_name);
			char *ext = strrchr(fname, '.');
			int dec = convertToDec(ext+1);

			for(int i=0; i<strlen(fname)-strlen(ext); i++) 
                clearPath[i] = fname[i];
			
			char *ext2 = strrchr(clearPath, '.');
			convertDecToBin(dec, bin, strlen(clearPath)-strlen(ext2));

            getDecimal(clearPath, bin, normalcase);
            sprintf(filePathDecimal,"%s/%s",path,normalcase);
            
            rename(filePath, filePathDecimal);
		}
	}
    closedir(dp);
}
```

pada fungsi ```convertToDec```, kita mereturn angka decimal, lalu kita ke for looping yang mana disini kita membuat variabel ```clearPath``` untuk menampung nama file yang akan terdecode. setelah itu kita cek kembali angka setelah titik terakhir sama kayak tadi dengan menggunakan variabel ```ext2```. setelah itu kita panggil fungsi ```convertDecToBin``` untuk mengubah desimal ke binary

```bash
int convertToDec(char *ext){
	int dec = 0; 
    int multiplier = 1;
	for(int i=strlen(ext)-1; i>=0; i--)
    {
        dec += (ext[i]-'0')*multiplier;
        multiplier *= 10;
    }
	return dec;
}
void convertDecToBin(int dec, char *bin, int len){
	int idx = 0;
	while(dec){
		if(dec & 1) bin[idx] = '1';
		else bin[idx] = '0';idx++;
		dec /= 2;
	}
	while(idx < len){
		bin[idx] = '0'; idx++;
	}
	bin[idx] = '\0';
	
	for(int i=0; i<idx/2; i++){
		char sementara = bin[i];bin[i] = bin[idx-1-i];bin[idx-1-i] = sementara;
	}
}
void getDecimal(char *filename, char *bin, char *normalcase){
	int end = strlen(filename);
	int begin = findSlash(filename, 0);
	int i;
	
	for(i=begin; i<end; i++){
		if(bin[i-begin] == '1') normalcase[i-begin] = filename[i] - 32;
		else normalcase[i-begin] = filename[i];
	}
	
	for(; i<strlen(filename); i++){
		normalcase[i-begin] = filename[i];
	}
	normalcase[i-begin] = '\0';

}
```

setelah mengkonvert decimal ke binary, kita panggil fungsi ```getDecimal``` untuk mengambil nilai desimal terus merename file yang sebelumnya uppercase insensitive dan diberi ekstensi baru berupa nilai desimal dari biner perbedaan namanya menjadi file biasa tanpa adanya hal tersebut

### Kendala
Fungsi yang ditulis belum sepenuhnya berjalan sesuai dengan keinginan pada saat dijalankan
